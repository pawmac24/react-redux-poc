const todos = (state = [], action) => {

    console.log("action.type = " + action.type);
    console.log("BEFORE state = " + JSON.stringify(state));
    switch (action.type) {
        case 'ADD_TODO': {
            let newState = [
                ...state,
                {
                    id: action.id,
                    text: action.text,
                    completed: false
                }
            ];
            console.log("AFTER state = " + JSON.stringify(newState));
            return newState;
        }
        case 'TOGGLE_TODO': {
            let newState = state.map(todo =>
                todo.id === action.id ? {...todo, completed: !todo.completed} : todo
            );
            console.log("AFTER state = " + JSON.stringify(newState));
            return newState;
        }
        default:
            return state;
    }
};

export default todos;